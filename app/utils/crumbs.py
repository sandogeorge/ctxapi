"""Breadcrumb constructors."""
from flask import (
    request, url_for,
)

from flask_login import (
    current_user,
)

from .models import (
    Flag, User,
)

def example(*args, **kwargs):
    """Example breadcrumb constructor for reference."""
    crumbs = [
        {'text': 'Crumb Example'}
    ]
    return crumbs


def edit_flag(*args, **kwargs):
    id = request.view_args['id']
    flag = Flag.query.filter_by(id=id).first()
    crumbs = [
        {'text': 'Edit ' + flag.name}
    ]
    return crumbs
