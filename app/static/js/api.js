var KAPI = (function (self, $) {
	
	// API endpoints.
	self.endpoints = {};
	
	// Initialize private properties.
	self.init = function(params) {
		self.endpoints = params.endpoints;
		
		return self;
	};
	
	return self;
}(KAPI || {}, jQuery));