"""Implements endpoints for the CTX API version 1.0."""


from http import HTTPStatus
import random

from bson.objectid import ObjectId
from bson.errors import InvalidId
from flask import (
    current_app, jsonify, request,
)
import validictory

from app import (
    app, oauth_provider as op,
)
from app.api_v1_0 import api_v1_0
from app.api_v1_0.schemata import schemata
from app.utils.decorators import async


@api_v1_0.route('/', methods=['GET'])
def index():
    """Dummy endpoint.
    Makes it easy to get base url in other parts of the app.
    """
    return jsonify({
        'status': HTTPStatus.OK,
        'message': 'API version 1.0'
    }), HTTPStatus.OK