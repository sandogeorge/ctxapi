"""Implements forms used in the configuration ui."""

from flask_wtf import FlaskForm

from wtforms import (
    BooleanField,
    StringField,
    SubmitField,
    ValidationError,
)
from wtforms.fields.html5 import EmailField
from wtforms.validators import (
    Email,
    Length,
    Required,
)

from app.utils.models import (
    User,
)


class FlagsForm(FlaskForm):
    submit = SubmitField('Save Changes')


class AddFlagForm(FlaskForm):
    name = StringField('Name', validators=[Length(max=32)])
    description = StringField(
        'Description', validators=[Length(max=64)])
    value = BooleanField('value')
    submit = SubmitField('Create Flag')


class EditFlagForm(FlaskForm):
    name = StringField('Name', validators=[Length(max=32)])
    description = StringField(
        'Description', validators=[Length(max=64)])
    value = BooleanField('value')
    submit = SubmitField('Save Changes')


class InviteUserForm(FlaskForm):
    name = StringField('Name', validators=[Required()])
    email = EmailField('Email address', validators=[Required(), Email()])
    submit = SubmitField('Send Invitation')

    @staticmethod
    def validate_email(dummy_form, field):
        """Check that the supplied email does not exist in the system."""
        user = User.query.filter_by(email=field.data).first()
        if user is not None:
            raise ValidationError('There is already an account associated \
                                  with the supplied email address.')


class EditUserForm(FlaskForm):
    """Form used to edit user details."""
    username = StringField('Username')
    email = EmailField('Email address')
    blocked = BooleanField('Blocked?')

    submit = SubmitField('Submit Changes')
