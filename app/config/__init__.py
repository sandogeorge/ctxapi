"""Implements the configuration ui."""

__all__ = ['config']

from flask import Blueprint

config = Blueprint('config', __name__)

from . import (
    forms, views,
)
