"""Implements configuration endpoints."""

from datetime import datetime

from flask import (
    flash,
    redirect,
    render_template,
    url_for,
)
from flask_breadcrumbs import register_breadcrumb
from flask_login import (
    current_user,
    login_required,
)

from wtforms import BooleanField

from app import sql
from app.config import config
from app.config.forms import (
    AddFlagForm,
    EditFlagForm,
    FlagsForm,
    EditUserForm,
    InviteUserForm,
)
from app.confirm import confirm_required
from app.utils.crumbs import edit_flag
from app.utils.decorators import (
    confirmed_required,
    admin_required,
)
from app.utils.mail import send_mail
from app.utils.models import (
    Flag,
    User,
    UserInvitation,
)


@config.route('/')
@login_required
@confirmed_required
@admin_required
@register_breadcrumb(config, 'bc.config', 'Configuration')
def index():
    page_vars = {
        'sidebar_class': ' two-column',
        'sidebar_right': True,
        'navwell': True,
        'title': 'Site Configuration',
        'page_header': 'Site Configuration'
    }
    return render_template('config/index.html', **page_vars)


@config.route('/flags', methods=['GET', 'POST'])
@login_required
@confirmed_required
@admin_required
@register_breadcrumb(config, 'bc.config.flags', 'System Flags')
def flags():
    flags = Flag.query.all()
    flag_ids = {}

    for flag in flags:
        setattr(FlagsForm, flag.name, BooleanField(flag.name))
        flag_ids[flag.name] = flag.id

    form = FlagsForm()

    if form.validate_on_submit():
        for flag in flags:
            flag.value = form[flag.name].data
            flag.last_modified = datetime.now()
            sql.session.add(flag)

        sql.session.commit()

        flash('Changes saved.', 'success')

    for flag in flags:
        form[flag.name].data = flag.value
        form[flag.name].description = flag.description

    page_vars = {
        'navwell': True,
        'title': 'System Flags',
        'page_header': 'Configure System Flags',
        'form': form,
        'flag_ids': flag_ids
    }
    return render_template('config/flags.html', **page_vars)


@config.route('/flags/add', methods=['GET', 'POST'])
@login_required
@confirmed_required
@admin_required
@register_breadcrumb(config, 'bc.config.flags.add', 'New Flag')
def add_flag():
    form = AddFlagForm()

    if form.validate_on_submit():
        flag = Flag(
            name=form.name.data,
            description=form.description.data,
            value=form.value.data,
            last_modified=datetime.now()
        )

        sql.session.add(flag)
        sql.session.commit()

        flash('New flag `{0}` created.'.format(flag.name), 'success')
        return redirect(url_for('config.flags'))

    page_vars = {
        'navwell': True,
        'title': 'New Flag',
        'page_header': 'Create a New Flag',
        'form': form
    }
    return render_template('config/addflag.html', **page_vars)


@config.route('/flags/<int:fid>/edit', methods=['GET', 'POST'])
@login_required
@confirmed_required
@admin_required
@register_breadcrumb(config, 'bc.config.flags.edit', '', dynamic_list_constructor=edit_flag)
def edit_flag(fid):
    flag = Flag.query.filter_by(id=fid).first()
    form = EditFlagForm()

    if form.validate_on_submit():
        flag.name = form.name.data
        flag.description = form.description.data
        flag.value = form.value.data
        flag.last_modified = datetime.now()

        sql.session.add(flag)
        sql.session.commit()

        flash('Flag `{0}` updated.'.format(flag.name), 'success')
        return redirect(url_for('config.flags'))

    form.name.data = flag.name
    form.description.data = flag.description
    form.value.data = flag.value

    page_vars = {
        'navwell': True,
        'title': 'Edit Flag',
        'page_header': 'Edit ' + flag.name,
        'form': form,
        'flag': flag
    }
    return render_template('config/editflag.html', **page_vars)


@config.route('/flags/<int:fid>/delete', methods=['GET', 'POST'])
@login_required
@confirmed_required
@admin_required
@confirm_required(
    'Are you sure you want to delete this flag?',
    'I am sure', 'config.edit_flag', ['fid'], 'danger'
)
def delete_flag(fid):
    flag = Flag.query.filter_by(id=fid).first()
    delattr(FlagsForm, flag.name)
    sql.session.delete(flag)
    sql.session.commit()
    flash('Flag `{0}` has been deleted.'.format(flag.name), 'success')
    return redirect(url_for('config.flags'))


@config.route('/users', methods=['GET', 'POST'])
@login_required
@confirmed_required
@admin_required
@register_breadcrumb(config, 'bc.config.users', 'Manage Users')
def users():
    users = User.query.all()

    page_vars = {
        'title': 'Manage Users',
        'navwell': True,
        'page_header': 'Manage Users',
        'users': users
    }
    return render_template('config/users.html', **page_vars)


@config.route('/users/invite', methods=['GET', 'POST'])
@login_required
@confirmed_required
@admin_required
@register_breadcrumb(config, 'bc.config.users.invite', 'Invite User')
def invite_user():
    form = InviteUserForm()

    if form.validate_on_submit():
        inv = UserInvitation(
            name=form.name.data,
            email=form.email.data,
            token=UserInvitation.generate_invitation_token(),
            sent_by=current_user.id
        )
        reg_url = url_for('auth.register', token=inv.token, _external=True)

        mail_vars = {
            'name': inv.name,
            'site_name': 'PhaseMetrics',
            'registration_token': inv.token,
            'registration_url': reg_url
        }
        text = render_template('mail/user-invitation.txt', vars=mail_vars)
        html = render_template('mail/user-invitation.html', vars=mail_vars)

        subject = 'Your Invitation to Register at PhaseMetrics'
        send_mail(subject, recipients=[inv.email], text=text, html=html)
        inv.sent_on = datetime.now()
        sql.session.add(inv)
        sql.session.commit()

        flash('An invitation to register has been sent to {0}.'
              .format(inv.email), 'success')
        return redirect(url_for('config.users'))

    page_vars = {
        'title': 'Invite User',
        'navwell': True,
        'page_header': 'Invite User',
        'form': form
    }
    return render_template('config/inviteuser.html', **page_vars)


@config.route('/users/<int:uid>/edit', methods=['GET', 'POST'])
@admin_required
@register_breadcrumb(config, 'bc.config.users.edit', 'Edit User')
def edit_user(uid):
    """Present form to edit user details."""
    user = User.query.filter_by(id=uid).first()

    form = EditUserForm()

    if form.validate_on_submit():
        if form.blocked.data:
            user.active = False
            user.deactivated_on = datetime.now()
        else:
            user.active = True
            user.deactivated_on = None

        sql.session.add(user)
        sql.session.commit()
        flash('User details updated.', 'success')
        return redirect(url_for('config.edit_user', uid=uid))

    form.username.data = user.username
    form.email.data = user.email
    form.blocked.data = not user.active

    page_vars = {
        'title': 'Edit User',
        'navwell': True,
        'page_header': 'Edit User',
        'form': form
    }
    return render_template('config/edituser.html', **page_vars)
